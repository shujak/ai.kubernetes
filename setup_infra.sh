#!/bin/bash

#Satisfy the external dependency on EPEL for DKMS and enable any optional repos for RHEL 8 only.
dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
ARCH=$( /bin/arch )
subscription-manager repos --enable codeready-builder-for-rhel-8-${ARCH}-rpms --enable rhel-8-for-${ARCH}-baseos-rpms --enable rhel-8-for-${ARCH}-appstream-rpms
distribution=$(. /etc/os-release;echo $ID`rpm -E "%{?rhel}%{?fedora}"`)
dnf config-manager --add-repo http://developer.download.nvidia.com/compute/cuda/repos/$distribution/${ARCH}/cuda-rhel8.repo
dnf install -y kernel-devel-$(uname -r) kernel-headers-$(uname -r)
dnf module install nvidia-driver:latest-dkms/default

#Install podman as a replacement for docker 
#since rhel8.5 doesn't support it
yum -y install podman
yum -y install kernel-devel-`uname -r` kernel-headers-`uname -r`
yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
yum -y install dkms
yum -y install http://developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/cuda-repo-rhel8-10.2.89-1.x86_64.rpm
modprobe -r nouveau

#Install cuda and check installation
yum -y install cuda
nvidia-smi --query-gpu=gpu_name --format=csv,noheader --id=0 | sed -e 's/ /-/g'

#Add distribution specific cuda-toolkit
distribution='rhel8.4'
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo | tee /etc/yum.repos.d/nvidia-docker.repo
yum -y install nvidia-container-toolkit

#Allow persistence mode for GPUs
#(without this, the docker/podman command will time-out)
nvidia-persistenced --persistence-mode

#Test the installation
podman run -e NVIDIA_VISIBLE_DEVICES=all nvidia/cuda:10.0-base nvidia-smi
