#!/bin/bash

yum install docker kubernetes-client kubernetes-node etcd
#systemctl disable firewalld
#systemctl stop firewalld

# Install local registry
yum install docker-distribution
systemctl start docker-distribution
systemctl enable docker-distribution
systemctl is-active docker-distribution

docker images

# Tag and assign images here

# Pull the docker containers from the Redhat registry
docker pull registry.access.redhat.com/rhel7/kubernetes-apiserver
docker pull registry.access.redhat.com/rhel7/kubernetes-controller-mgr
docker pull registry.access.redhat.com/rhel7/kubernetes-scheduler
