# Install tigerVNC and its pre-requisites
yum -y install tigervnc-server tigervnc
yum -y group install GNOME base-x

# If a graphical desktop environment is not already installed, the following package groups need to be installed to boot a RHEL system into a graphical desktop environment
yum groupinstall "Server with GUI"

# Set the system to boot directly into the GUI then switch to graphical.target
systemctl set-default graphical.target
systemctl isolate graphical.target

# Create VNC specific password
useradd $1
passwd $1
su - $1
vncpasswd

echo 'session=gnome' > ~/.vnc/config

# Add each new user to this file
echo ':1='$1 >> /etc/tigervnc/vncserver.users

# Create systemd unit for VNC server@:1
cp /lib/systemd/system/vncserver@.service /etc/systemd/system/vncserver@:1.service

# Configure the firewall to only allow connections on port 5901
firewall-cmd --permanent --zone=public --add-port 5901/tcp
firewall-cmd  --reload

# Enable the service
systemctl enable vncserver@:1.service

# Check server status 
systemctl start vncserver@:1.service

# Also check port 5901 to see if the server is running
ss -atp | grep 5901

